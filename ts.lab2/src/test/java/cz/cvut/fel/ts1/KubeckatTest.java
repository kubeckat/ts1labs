package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class KubeckatTest {
    @Test
    public void factorialTest(){
        Kubeckat kubeckat = new Kubeckat();
        int actual = kubeckat.factorial(5);
        Assertions.assertEquals(actual, 120);
    }
    @Test
    public void factorialTest0(){
        Kubeckat kubeckat = new Kubeckat();
        int actual = kubeckat.factorial(0);
        Assertions.assertEquals(actual, 1);
    }
    @Test
    public void factorialTest1(){
        Kubeckat kubeckat = new Kubeckat();
        int actual = kubeckat.factorial(1);
        Assertions.assertEquals(actual, 1);
    }
}
