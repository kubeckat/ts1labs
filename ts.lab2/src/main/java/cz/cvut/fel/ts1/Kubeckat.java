package cz.cvut.fel.ts1;

public class Kubeckat {
    public int factorial(int n){
        int ret=1;
        for (int i =1; i<=n; i++){
            ret=ret*i;
        }
        return ret;
    }
}
