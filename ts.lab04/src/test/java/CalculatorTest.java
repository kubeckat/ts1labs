import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class CalculatorTest {
    private static Calculator calculator;

    @BeforeAll
    static void start(){
        calculator = new Calculator();
    }

    @Test
    public void additionTest_6plus5_return11() {
        int a = 6;
        int b = 5;
        int expected = 11;
        int result = calculator.add(a, b);
        Assertions.assertEquals(expected, result);
    }

    @Test
    void subtractTest_6minus5_return1() {
        int a = 6;
        int b = 5;
        int expected = 1;
        int result = calculator.subtract(a, b);
        Assertions.assertEquals(expected, result);
    }

    @Test
    void multiplyTest_6by5_return30() {
        int a = 6;
        int b = 5;
        int expected = 30;
        int result = calculator.multiply(a, b);
        Assertions.assertEquals(expected, result);
    }

    @Test
    void divideTest_10divided2_return5() {
        int a = 10;
        int b = 2;
        int expected = 5;
        int result = calculator.divide(a, b);
        Assertions.assertEquals(expected, result);
    }
    @Test
    void divideTest_10divided0_thromError() {
        int a = 10;
        int b = 0;
        Assertions.assertThrows(ArithmeticException.class, ()->calculator.divide(a,b));
    }
}